<!DOCTYPE html>
<html>
<head>
<title>Beat 21</title>
</head>
<body>

<?php

class player {
	private $point=0;
	private $drawed_card_list = array();

	public function add_point($values) {
		array_push($this->drawed_card_list, $values);
		# Modify the card to only the contained value
		$tempval = substr($values,1,2);

		switch ($tempval) {
			case '1': $this->point += intval($tempval); break;
			case '2': $this->point += intval($tempval); break;
			case '3': $this->point += intval($tempval); break;
			case '4': $this->point += intval($tempval); break;
			case '5': $this->point += intval($tempval); break;
			case '6': $this->point += intval($tempval); break;
			case '7': $this->point += intval($tempval); break;
			case '8': $this->point += intval($tempval); break;
			case '9': $this->point += intval($tempval); break;

			case 'A':
				$this->point += 11;
				break;

			default:
				$this->point += 10;
				break;
		}
	}

	public function show_point() {
		return $this->point;
	}

	public function show_cards() {
		foreach ($this->drawed_card_list as $key => $value) {
			echo ($key==0) ? $value : ", " . $value;
		}		
	}
}

class deck {
	private $suits = array("C","D","H","S");
	private $values = array("2","3","4","5","6","7","8","9","10","J","Q","K","A");
	public $pakli = array();

	function __construct() {
		$this->exists_deck_check();		
	}

	public function exists_deck_check() {		
		$file = 'custom_deck.txt';
		# If file exists then
		if (file_exists($file)) {
		    
		    $teszt = fopen($file, "r");
		    $temp_str = fgets($teszt);
		    $this->pakli = explode(", ", $temp_str);

		    # If file not contains exactly 52 cards
		    if (count($this->pakli)===51) {		    	
		    	echo "Your deck not contains 52 card!";
		    }

		    fclose($teszt);

		} else { # If no found file, then automatically create a new deck
		    foreach ($this->suits as $key => $value) {
		    	foreach ($this->values as $key1 => $value1) {		    		
		    		array_push($this->pakli,$value.$value1);
		    	}
		    }
		    # Shuffle the deck if file not exists
		    $this->shuffle();
		}
	}	

	public function shuffle() {
		shuffle($this->pakli);
		# Need reverse order because you draw form a top of your deck.
		array_reverse($this->pakli);
	}

	public function draw() {
		return array_pop($this->pakli);
	}

	public function show_deck() {
		echo "<br><pre>";
		print_r($this->pakli);
		echo "<br></pre>";
	}

}

$winnner_name = null;
$new_deck = new deck();
$dealer = new player();
$sam = new player();

//$new_deck->show_deck();

# Game start, both player draw each-each card from the deck
$sam->add_point($new_deck->draw());
$dealer->add_point($new_deck->draw());
$sam->add_point($new_deck->draw());
$dealer->add_point($new_deck->draw());

# Play game, Draw each player while the conditions are not true
# Instant A+A rule
if ($sam->show_point()>21 && $dealer->show_point()>21) {
	$winnner_name = "dealer";	
}

# Instant Blackjack rule
if ($sam->show_point()==21 && $dealer->show_point()==21) {
	$winnner_name = "sam";	
} elseif ($sam->show_point()==21) {
	$winnner_name = "sam";	
} elseif ($dealer->show_point()==21) {
	$winnner_name = "dealer";	
} else {
	
	# Sam start drawing	
	$stop_sam = true;
	$stop_dealer = true;
	
	while ($stop_sam || $stop_dealer) {
		
		if ($sam->show_point()>=17 || $dealer->show_point()>=21) {
			$stop_sam=false;		
		} else {
			$sam->add_point($new_deck->draw());		
		}		

		if ($dealer->show_point()>$sam->show_point() || $dealer->show_point()>=21 || $sam->show_point()>=21) {
			$stop_dealer=false;			
		} else {
			$dealer->add_point($new_deck->draw());		
		}	
	}

	# Decide who wins
	if ($sam->show_point()>$dealer->show_point() && $sam->show_point()<=21) {
		$winnner_name = "sam";
	} elseif ($sam->show_point()<$dealer->show_point()) {
		if ($dealer->show_point()<=21) {
			$winnner_name = "dealer";
		} else {
			$winnner_name = "sam";	
		}
	} elseif ($dealer->show_point()<=21) {
		$winnner_name = "dealer";
	}
}

# End game, Write the winner name and the drawed cards.
echo $winnner_name;
echo "<br>sam: ";
$sam->show_cards();
echo "<br>dealer: ";
$dealer->show_cards();

?>

</body>
</html>